package forza4;

class Concorrente implements Comparable {

    private String nome, vince, perde;

    public Concorrente() {
    }

    public void setNome(String x) {
        this.nome = x;
    }

    public void setPerde(String x) {
        this.perde = x;
    }

    public void setVince(String x) {
        this.vince = x;
    }

    public String getNome() {
        return this.nome;
    }

    public String getVince() {
        return this.vince;
    }

    public String getPerde() {
        return this.perde;
    }

    public String toC() {
        return this.nome + ";" + this.vince + ";" + this.perde;
    }

    @Override
    public String toString() {

        return this.vince;
    }

    public int compareTo(Object o) {
        Concorrente t = (Concorrente) o;
        return vince.compareTo(t.vince);
    }
}
