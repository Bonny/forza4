package forza4;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.io.*;
import java.util.*;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class Registra {

    public Registra() {
    }

    public void addConcorrenti(String nomevince, String nomeperde) throws FileNotFoundException, IOException {

        List<Concorrente> list = new ArrayList<Concorrente>();
        Concorrente c;
        FileReader f = new FileReader("foo.txt");
        BufferedReader fin = new BufferedReader(f);
        StringTokenizer st;

        String s = fin.readLine();

        while (s != null) {
            st = new StringTokenizer(s, ";");
            c = new Concorrente();
            c.setNome(st.nextToken());
            c.setVince(st.nextToken());
            c.setPerde(st.nextToken());
            list.add(c);
            s = fin.readLine();
        }
        fin.close();
        //Concorrente cv[] = list.toArray(new Concorrente[list.size()]);
        boolean flagvince = false, flagperde = false;

        for (Object x : list) {
            Concorrente t = (Concorrente) x;
            if (t.getNome().equals(nomevince)) {
                t.setVince(String.valueOf(Integer.parseInt(t.getVince()) + 1));
                flagvince = true;
            } else if (t.getNome().equals(nomeperde)) {
                t.setPerde(String.valueOf(Integer.parseInt(t.getPerde()) + 1));
                flagperde = true;
            }
        }

        if (flagvince == true && flagperde == true) {
            stampa(list);
        } else if (flagvince == true && flagperde == false) {
            c = new Concorrente();
            c.setNome(nomeperde);
            c.setVince("0");
            c.setPerde("1");
            list.add(c);
            stampa(list);
        } else if (flagvince == false && flagperde == true) {
            c = new Concorrente();
            c.setNome(nomevince);
            c.setVince("1");
            c.setPerde("0");
            list.add(c);
            stampa(list);
        } else if (flagvince == false && flagperde == false) {
            c = new Concorrente();
            c.setNome(nomeperde);
            c.setVince("0");
            c.setPerde("1");
            list.add(c);
            c = new Concorrente();
            c.setNome(nomevince);
            c.setVince("1");
            c.setPerde("0");
            list.add(c);
            stampa(list);
        }
    }

    private void stampa(List<Concorrente> list) throws IOException {

        FileWriter f = new FileWriter("foo.txt");
        PrintWriter fout = new PrintWriter(f);
        for (Object x : list) {
            Concorrente c = (Concorrente) x;
            fout.println(c.toC());
        }
        fout.close();
    }

    public void classifica() throws FileNotFoundException, IOException {

        List<Concorrente> list = new ArrayList<Concorrente>();
        Concorrente t;
        FileReader f = new FileReader("foo.txt");
        BufferedReader fin = new BufferedReader(f);
        StringTokenizer st;

        String s = fin.readLine();

        while (s != null) {
            st = new StringTokenizer(s, ";");
            t = new Concorrente();
            t.setNome(st.nextToken());
            t.setVince(st.nextToken());
            t.setPerde(st.nextToken());
            list.add(t);
            s = fin.readLine();
        }
        fin.close();

        Collections.sort(list);
        Collections.reverse(list);
        String titolicol[] = {"Nome", "Partite vinte", "Partite perse"};
        String m[][] = new String[list.size()][3];

        for (int i = 0; i < list.size(); i++) {
            t = list.get(i);
            m[i][0] = t.getNome();
            m[i][1] = t.getVince();
            m[i][2] = t.getPerde();
        }
        JTable table = new JTable(m, titolicol);
        JFrame frame = new JFrame();
        table.setFont(new Font("Arial", Font.BOLD, 14));
        table.setAutoscrolls(true);
        JScrollPane scrol = new JScrollPane(table);
        frame.getContentPane().add(scrol, "Center");
        frame.setVisible(true);

        Toolkit mioToolkit = Toolkit.getDefaultToolkit();
        Dimension dimensioniSchermo = mioToolkit.getScreenSize();
        frame.setBounds((int) (dimensioniSchermo.getWidth() / 2) - 175, (int) (dimensioniSchermo.getHeight() / 2) - 200, 350, 400);

    }
}
