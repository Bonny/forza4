
package forza4;

import javax.swing.JLabel;

    class Mylabel extends JLabel {

        private String stato;

        public Mylabel() {
            super("", JLabel.CENTER);
            this.stato = "";
        }

        public String getstato() {
            return this.stato;
        }

        public void setstato(String s) {
            this.stato = s;
        }
    }
