package forza4;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;

/**
 *
 * @author Bonny
 */
public final class Framef4 extends JFrame implements ActionListener {

    private String Player = "rosso";
    private Mylabel lbl[][] = new Mylabel[6][7];
    private JMenuBar barraDeiMenu;
    private JMenu menu1, menu2;
    private JMenuItem menuItem0, menuItem1, menuItem2, menuItem3;
    private JCheckBoxMenuItem menuItem4;
    private JLabel lblrosso, lblgiallo;
    private FrameNomi fn;
    private Registra reg = new Registra();

    /**
     *
     */
    public Framef4() {

        super("JForza 4");
        JPanel p = new JPanel();
        p.setLayout(new GridLayout(6, 7, 10, 10));
        p.setBackground(new Color(0, 0, 255));
        for (int i = 0; i < 6; i++) {
            for (int k = 0; k < 7; k++) {
                p.add(lbl[i][k] = new Mylabel());
                lbl[i][k].setName(String.valueOf(i) + String.valueOf(k));
                lbl[i][k].setIcon(new ImageIcon("img/vuoto.jpg"));
                lbl[i][k].setstato("");
                lbl[i][k].addMouseListener(new Gestore());
            }
        }
        this.getContentPane().add(p, "Center");
        barraDeiMenu = new JMenuBar();
        menu1 = new JMenu("Player");
        menu2 = new JMenu("Sound");
        menuItem0 = new JMenuItem("Nuova partita - cambio concorrenti");
        menuItem1 = new JMenuItem("Nuova partita");
        menuItem2 = new JMenuItem("Classifica");
        menuItem3 = new JMenuItem("Esci");
        menuItem4 = new JCheckBoxMenuItem("Abilita audio");
        menuItem0.addActionListener(this);
        menuItem1.addActionListener(this);
        menuItem2.addActionListener(this);
        menuItem3.addActionListener(this);
        menu1.add(menuItem0);
        menu1.add(menuItem1);
        menu1.add(menuItem2);
        menu1.add(menuItem3);
        menu2.add(menuItem4);
        barraDeiMenu.add(menu1);
        barraDeiMenu.add(menu2);
        this.setJMenuBar(barraDeiMenu);
        int audio = JOptionPane.showConfirmDialog(null, "Audio??", "Effetti sonori", JOptionPane.YES_NO_OPTION);
        if (audio == 0) {
            menuItem4.setState(true);
        } else {
            menuItem4.setState(false);
        }
        try {
            if (menuItem4.getState()) {
                playSound("soundtrack/init.wav");
            }
        } catch (Exception ex) {
            System.err.println();
        }

        JPanel plbl = new JPanel();
        plbl.setLayout(new GridLayout(2, 1, 5, 5));
        lblrosso = new JLabel();
        lblrosso.setFont(new Font("Arial", Font.BOLD, 18));
        lblrosso.setIcon(new ImageIcon("img/iconred.jpg"));
        lblgiallo = new JLabel();
        lblgiallo.setFont(new Font("Arial", Font.BOLD, 18));
        lblgiallo.setIcon(new ImageIcon("img/icongiallo.jpg"));
        plbl.add(lblrosso);
        plbl.add(lblgiallo);
        this.getContentPane().add(plbl, "North");
        Image im = Toolkit.getDefaultToolkit().getImage("img/icon1.gif");
        this.setIconImage(im);
        setResizable(false);
        setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Toolkit mioToolkit = Toolkit.getDefaultToolkit();
        Dimension dimensioniSchermo = mioToolkit.getScreenSize();
        this.setBounds((int) (dimensioniSchermo.getWidth() / 2) - 300, (int) (dimensioniSchermo.getHeight() / 2) - 300, 600, 600);
    }

    public void actionPerformed(ActionEvent evt) {
        String e = evt.getActionCommand();
        if (e.equals("Esci")) {
            System.exit(0);
        } else if (e.equals("Nuova partita")) {
            if (lblrosso.getText().equals("")) {
                fn = new FrameNomi();

            } else {
                initGame();
            }

        } else if (e.equals("Nuova partita - cambio concorrenti")) {
            fn = new FrameNomi();

        } else if (e.equals("Classifica")) {
            try {
                if (menuItem4.getState()) {
                    playSound("soundtrack/win.wav");
                    reg.classifica();
                }
            } catch (Exception ex) {
            }
        }
    }

    /**
     *
     * @param P
     * @throws FileNotFoundException
     * @throws IOException
     */
    public void control(String P) throws FileNotFoundException, IOException {// metodo che costruisce le sotto-matrici 4x4 dalla matrice 6x7
        // e determina il vincitore
        final int rows = 3;
        final int cols = 3;
        boolean flag = false;
        int j = 0;
        String temp[][] = new String[4][4];
        String tempindex[][] = new String[4][4];
////////////////////////////////////////////////
        for (int i = 0; i < rows; i++) {

            j = 0;

            for (; j <= cols; j++) {

                for (int m = i; m <= rows + i; m++) {

                    for (int n = j; n <= cols + j; n++) {

                        temp[m - i][n - j] = lbl[m][n].getstato();
                        tempindex[m - i][n - j] = lbl[m][n].getName();

                    }// n
                }//m
                if (win(temp, tempindex, P) == true) {
                    flag = true;
                }
            }//j
        }//i
///////////////////////////////////////////////
        if (flag == true) {
            try {
                if (menuItem4.getState()) {
                    playSound("soundtrack/winner.wav");
                }
            } catch (Exception ex) {
                System.err.println();
            }
            if (Player.equals("rosso")) {
                reg.addConcorrenti(lblrosso.getText(), lblgiallo.getText());
                JOptionPane.showMessageDialog(null, lblrosso.getText() + " ha vinto!!", "JForza4", JOptionPane.INFORMATION_MESSAGE);
            } else {
                reg.addConcorrenti(lblgiallo.getText(), lblrosso.getText());
                JOptionPane.showMessageDialog(null, lblgiallo.getText() + " ha vinto!!", "JForza4", JOptionPane.INFORMATION_MESSAGE);
            }
            initGame();

        } else if (flag == false) {

            if (P.equals("rosso")) {
                Player = "giallo";
                lblgiallo.setForeground(Color.orange);
                lblrosso.setForeground(Color.BLACK);
            } else {
                Player = "rosso";
                lblgiallo.setForeground(Color.BLACK);
                lblrosso.setForeground(Color.orange);
            }

        }
    }//fine metodo control

    /**
     *
     */
    public void initGame() {
        try {
            if (menuItem4.getState()) {
                playSound("soundtrack/s.wav");
            }
        } catch (Exception ex) {
        }
        lblgiallo.setForeground(Color.BLACK);
        lblrosso.setForeground(Color.orange);
        Player = "rosso";
        for (int i = 0; i < 6; i++) {
            for (int k = 0; k < 7; k++) {
                lbl[i][k].setstato("");
                lbl[i][k].setIcon(new ImageIcon("img/vuoto.jpg"));
            }
        }
    }

    /**
     *
     * @param t
     * @param tindex
     * @param p
     * @return
     */
    public boolean win(String t[][], String tindex[][], String p) {//metodo controllo sotto-matrice 4x4
//nelle selezioni devo fare almeno un controllo con p perchÃ¨ all'inizio la proprieta stato di ogni
//elemento della matrice lbl[][] sono tutte = ""

        //controllo righe e colonne
        for (int i = 0; i < 4; i++) {
            if ((t[i][0].equals(t[i][1]) && t[i][1].equals(t[i][2]) && t[i][2].equals(t[i][3])) && t[i][0].equals(p)) {

                evidenziaW(tindex[i][0], tindex[i][1], tindex[i][2], tindex[i][3], p);
                return true;
            }
            if ((t[0][i].equals(t[1][i]) && t[1][i].equals(t[2][i]) && t[2][i].equals(t[3][i])) && t[0][i].equals(p)) {

                evidenziaW(tindex[0][i], tindex[1][i], tindex[2][i], tindex[3][i], p);
                return true;
            }
        }
        //diagonale principale
        if ((t[0][0].equals(t[1][1]) && t[1][1].equals(t[2][2]) && t[2][2].equals(t[3][3])) && t[0][0].equals(p)) {

            evidenziaW(tindex[0][0], tindex[1][1], tindex[2][2], tindex[3][3], p);
            return true;
        }
        //diagonale secondaria
        if ((t[3][0].equals(t[2][1]) && t[2][1].equals(t[1][2]) && t[1][2].equals(t[0][3])) && t[0][3].equals(p)) {

            evidenziaW(tindex[0][3], tindex[1][2], tindex[2][1], tindex[3][0], p);
            return true;
        }
        return false;
    }

    /**
     *
     * @param cella1
     * @param cella2
     * @param cella3
     * @param cella4
     * @param p
     */
    public void evidenziaW(String cella1, String cella2, String cella3, String cella4, String p) {
        lbl[Integer.parseInt(cella1.substring(0, 1))][Integer.parseInt(cella1.substring(1, 2))].setIcon(new ImageIcon("img/" + p + "w.jpg"));
        lbl[Integer.parseInt(cella2.substring(0, 1))][Integer.parseInt(cella2.substring(1, 2))].setIcon(new ImageIcon("img/" + p + "w.jpg"));
        lbl[Integer.parseInt(cella3.substring(0, 1))][Integer.parseInt(cella3.substring(1, 2))].setIcon(new ImageIcon("img/" + p + "w.jpg"));
        lbl[Integer.parseInt(cella4.substring(0, 1))][Integer.parseInt(cella4.substring(1, 2))].setIcon(new ImageIcon("img/" + p + "w.jpg"));
    }

    class Gestore implements MouseListener {

        JLabel l;

        public void mouseClicked(MouseEvent e) {

            if (!lblrosso.getText().equals("")) {
                try {
                    if (menuItem4.getState()) {
                        playSound("soundtrack/bip.wav");
                    }
                } catch (Exception ex) {
                    System.err.println();
                }
                l = (JLabel) e.getComponent();

                int index = Integer.parseInt(l.getName().substring(1, 2));
                int c = 5;

                while (c >= 0) {

                    if (lbl[c][index].getstato().equals("")) {
                        if (Player.equals("rosso")) {
                            lbl[c][index].setIcon(new ImageIcon("img/rosso.jpg"));
                            lbl[c][index].setstato(Player);
                            try {
                                control(Player);
                            } catch (Exception ex) {
                                System.err.println();
                            }
                            break;
                        } else if (Player.equals("giallo")) {
                            lbl[c][index].setIcon(new ImageIcon("img/giallo.jpg"));
                            lbl[c][index].setstato(Player);
                            try {
                                control(Player);
                            } catch (Exception ex) {
                                System.err.println();
                            }
                            break;
                        }
                    } else {
                        c--;
                    }
                }
            } else {
                try {
                    if (menuItem4.getState()) {
                        playSound("soundtrack/fuel.wav");
                    }
                } catch (Exception ex) {
                    System.err.println();
                }
                JOptionPane.showMessageDialog(null, "Attenzione devi inserire i nomi dei concorrenti\n per iniziare il gioco", "Attenzione", JOptionPane.ERROR_MESSAGE);
            }
        }

        public void mousePressed(MouseEvent e) {
        }

        public void mouseReleased(MouseEvent e) {
        }

        public void mouseEntered(MouseEvent e) {
        }

        public void mouseExited(MouseEvent e) {
        }
    }

    class FrameNomi extends JFrame {

        private JTextField red, yellow;
        private JButton but;
        private JLabel lblrosso1, lblgiallo1;

        public FrameNomi() {
            super("JForza 4");
            red = new JTextField();
            red.setFont(new Font("Arial", Font.BOLD, 16));
            yellow = new JTextField();
            yellow.setFont(new Font("Arial", Font.BOLD, 16));
            but = new JButton("Ok");
            lblrosso1 = new JLabel();
            lblrosso1.setIcon(new ImageIcon("img/iconred.jpg"));
            lblgiallo1 = new JLabel();
            lblgiallo1.setIcon(new ImageIcon("img/icongiallo.jpg"));
            JPanel p = new JPanel();
            p.setLayout(new GridLayout(2, 2, 10, 10));
            p.add(lblrosso1);
            p.add(red);
            p.add(lblgiallo1);
            p.add(yellow);
            JLabel t = new JLabel("Inserire i nomi dei concorrenti");
            t.setFont(new Font("Arial", Font.BOLD, 18));
            this.getContentPane().add(t, "North");
            this.getContentPane().add(p, "Center");
            this.getContentPane().add(but, "South");
            Image im = Toolkit.getDefaultToolkit().getImage("img/icon1.gif");
            this.setIconImage(im);
            Toolkit mioToolkit = Toolkit.getDefaultToolkit();
            Dimension dimensioniSchermo = mioToolkit.getScreenSize();
            this.setBounds((int) (dimensioniSchermo.getWidth() / 2) - 150, (int) (dimensioniSchermo.getHeight() / 2) - 70, 300, 140);
            setResizable(false);
            setVisible(true);

            but.addActionListener(
                    new ActionListener() {

                        public void actionPerformed(ActionEvent event) {
                            String r = red.getText();
                            String y = yellow.getText();
                            if (r.equals("") || y.equals("") || r.equals(y)) {
                                try {
                                    if (menuItem4.getState()) {
                                        playSound("soundtrack/fuel.wav");
                                    }
                                } catch (Exception ex) {
                                    System.err.println();
                                }
                                JOptionPane.showMessageDialog(null, "Attenzione dati non corretti.", "Attenzione", JOptionPane.ERROR_MESSAGE);
                            } else {
                                lblrosso.setText(red.getText());
                                lblgiallo.setText(yellow.getText());
                                red.setText("");
                                yellow.setText("");
                                setVisible(false);
                                initGame();
                            }
                        }
                    });
        }
    }

    /**
     *
     * @param path
     * @throws FileNotFoundException
     * @throws IOException
     */
    public void playSound(String path) throws FileNotFoundException, IOException {

        InputStream in = new FileInputStream(path);

        AudioStream audioStream = new AudioStream(in);

        AudioPlayer.player.start(audioStream);
    }
}
